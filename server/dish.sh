#!/bin/sh
LOG_FILE="/data/maintain/log/disk.log"
DAY=`date +%d`
if [ $(( $DAY % 7 )) -eq 0 ]; then
    LOG_DAY=$((DAY-1))
    if [ $LOG_DAY -lt 10 ]; then
	RENAME="/data/maintain/log/disk_`date +%y%m`0$LOG_DAY.log"
    else
	RENAME="/data/maintain/log/disk_`date +%y%m`$LOG_DAY.log"
    fi
    if [ ! -f $RENAME ];then
	if [ -f $LOG_FILE ]; then
            mv $LOG_FILE $RENAME
        fi
    fi
fi


MAX_PERCENT=80
EMAIL=sondn.5442@gmail.com:sondn@appota.com
IFS=:
arr_mail=($EMAIL)
PART=dev/sda2
PERCENT=`df -h | grep $PART | awk '{ print $5 }' | cut -d'%' -f1`
USED=`df -h | grep $PART | awk '{ print $3 }'`
TOTAL=`df -h | grep $PART | awk '{ print $2 }'`
AVAILABLE=`df -h | grep $PART | awk '{ print $4 }'`
if [ $PERCENT -gt $MAX_PERCENT ]; then
    for keymail in "${!arr_mail[@]}"; do
	mail=${arr_mail[$keymail]}
	echo "[`date +%Y-%m-%d_%H:%M:%S`] Disk used $PERCENT%: Total: $TOTAL Used: $USED Available: $AVAILABLE" | mail -s "Running out of disk space host_name" $mail
    done
fi
echo "[`date +%Y-%m-%d_%H:%M:%S`] Disk used $PERCENT%: Total: $TOTAL Used: $USED Available: $AVAILABLE" >> $LOG_FILE

