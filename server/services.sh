#!/bin/sh
LOG_FILE="/data/maintain/log/services.log"
DAY=`date +%d`
if [ $(( ${DAY#0} % 7 )) -eq 0 ]; then
    LOG_DAY=$((DAY-1))
        if [ $LOG_DAY -lt 10 ]; then
            RENAME="/data/maintain/log/services_`date +%y%m`0$LOG_DAY.log"
        else
            RENAME="/data/maintain/log/services_`date +%y%m`$LOG_DAY.log"
        fi
        if [ ! -f $RENAME ];then
            if [ -f $LOG_FILE ]; then
                mv $LOG_FILE $RENAME
            fi
        fi
fi
EMAIL=sondn.5442@gmail.com:sondn@appota.com
SERVICES=mysql:nginx:php-fpm
IFS=:
arr=($SERVICES)
arr_mail=($EMAIL)
for key in "${!arr[@]}"; do
    service=${arr[$key]}
    status=`ps -ef | grep -v grep | grep $service | wc -l`
        if [ $status -gt 0 ]; then
            echo "[`date +%Y-%m-%d_%H:%M:%S`] $service is running" >> $LOG_FILE
        else
            echo "[`date +%Y-%m-%d_%H:%M:%S`] $service is not running" >> $LOG_FILE
            for keymail in "${!arr_mail[@]}"; do
                mail=${arr_mail[$keymail]}
                echo "[`date +%Y-%m-%d_%H:%M:%S`] $service is not running" | mail -s "$service is not running - host_name" $mail
            done
        fi
done

