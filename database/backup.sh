#!/bin/sh -e

BACKUP_NAME=/root/backup/mysql/`date +%Y%m%d_%H%M%S_db_name`.db

mysqldump -u username --password=password dbname > $BACKUP_NAME

gzip $BACKUP_NAME

scp $BACKUP_NAME.gz backup_user@backup_host:/data/backup/gamota-sdk

rm $BACKUP_NAME.gz
